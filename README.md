# This repo includes all our HOW TO related to the ESC Project

*New* => We have made some modifications in Mapper-Daemon to make pin mapping easier.

Instructions for update from the last image on google drive:
    
- Connect at ssh@*pi*: password *raspberry*
    
- Commands below:

        git clone https://gitlab.com/INSHack-CSAW2017/Demon-Mapper.git
        cd Demon-Mapper
        sudo ./reinstall.sh
        cd ..
        
        sudo reboot
        

## Pin mapping Explained:

In the "PinMapping" section of the website, you can make links beetwen physical
(GPIOS) pins and logical (isolated PLCs in commons groups). We didn't had the time
to make a good description on the website, so we will explain this process below:

Physical (On board): 

    - From 2 to 27 corresponding as "BCM" pins (image on PDF), each pin can be set as an input or an output
    - Pins 12 and 13 are reserved for integer outputs, you can set it with logical output pin 0
    
    
Logical (On PLCs):

    - Each number is corresponding to a specific input or output, depending on the direction you choose
    - If you have setted the pin 1 as input, you can set pin 1 as output too, because there are different
    - You can map a pin multiple time (be careful with the outputs)

    - Input direction:
        - 1 => %IX0.0
        - 2 => %IX0.1
        - 3 => %IX0.2
        - 4 => %IX0.3
        - 5 => %IX0.4
        - 6 => %IX0.5
        - 7 => %IX0.6
        - 8 => %IX0.7
        ...
    
    - Output direction:
        - 0 => Output (%QW0)
        - 1 => %QX0.0
        - 2 => %QX0.1
        - 3 => %QX0.2
        - 4 => %QX0.3
        - 5 => %QX0.4
        - 6 => %QX0.5
        - 7 => %QX0.6
        - 8 => %QX0.7
        ...